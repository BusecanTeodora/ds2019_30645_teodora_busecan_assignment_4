import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {LoginService} from './login.service';
import {Activity} from '../model/Activity';
import {LocalDate, LocalDateTime} from '@js-joda/core';
import {DrugStatus} from '../model/DrugStatus';
import {stringify} from 'querystring';
import {RecomandationDTO} from '../model/recomandation-dto.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private doctorUrl: string;
  private ass4Url: string;

  constructor(private http: HttpClient, private loginService: LoginService) {
   // this.doctorUrl = 'http://localhost:8080/doctor';
  }

   public getAllPatients() {
     const doctorId: User = JSON.parse(sessionStorage.getItem('loggedUser'));
     const headers = new HttpHeaders();
     headers.append('authorization', doctorId.role);
     return this.http.get<[Patient]>(`${this.doctorUrl}/${doctorId.id}/patients`, {headers});
   }
  
   public getAllMedicationPlans() {
     const doctorId: User = JSON.parse(sessionStorage.getItem('loggedUser'));
     const headers = new HttpHeaders();
     headers.append('authorization', doctorId.role);
     return this.http.get<[MedicationPlan]>(`${this.doctorUrl}/${doctorId.id}/medicationPlans`);
   }
  
   public getMedications() {
     return this.http.get<[Medication]>(`${this.doctorUrl}/medications`);
   }


}
