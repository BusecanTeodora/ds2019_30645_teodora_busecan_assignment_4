package com.example.springdemo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "drug")
public class Drug implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "iddrug", unique = true, nullable = false)
    private Integer iddrug;


    @Column(name = "name")
    private String name;

    @Column(name = "sideeffect")
    private String sideeffect;

    @Column(name = "dosage")
    private Integer dosage;


    public Drug() {
    }

    public Drug(Integer iddrug, String name, String sideeffect, Integer dosage) {
        this.iddrug = iddrug;
        this.name = name;
        this.sideeffect = sideeffect;
        this.dosage = dosage;
    }

    public Integer getIddrug() {
        return iddrug;
    }

    public void setIddrug(Integer iddrug) {
        this.iddrug = iddrug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideeffect() {
        return sideeffect;
    }

    public void setSideeffect(String sideeffect) {
        this.sideeffect = sideeffect;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
