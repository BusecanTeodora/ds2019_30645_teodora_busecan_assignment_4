package com.example.springdemo.dto;

import java.util.Objects;

public class UserDTO {

    private Integer iduser;
    private String username;
    private String password;

    private UserTypeDTO userTypeDTO;

    public UserDTO() {
    }

    public UserDTO(Integer iduser, String username, String password, UserTypeDTO userTypeDTO) {
        this.iduser = iduser;
        this.username = username;
        this.password = password;
        this.userTypeDTO = userTypeDTO;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserTypeDTO getUserTypeDTO() {
        return userTypeDTO;
    }

    public void setUserTypeDTO(UserTypeDTO userTypeDTO) {
        this.userTypeDTO = userTypeDTO;
    }

   public void setUT(Integer i){
        this.userTypeDTO.setUsertype(i);
   }
}
