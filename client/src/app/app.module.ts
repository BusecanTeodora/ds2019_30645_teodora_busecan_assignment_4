import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatButtonModule,
  MatTableModule,
  MatPaginatorModule,
  MatDialogModule
} from '@angular/material';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DrugsComponent} from './drug/drugs.component';
import {DrugModifyComponent} from './drug-modify/drug-modify.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ErrorHandlingComponent } from './error-handling/error-handling.component';
import { CaregiverPageComponent } from './caregiver-page/caregiver-page.component';
import { PatientPageComponent } from './patient-page/patient-page.component';
import { DoctorPageComponent } from './doctor-page/doctor-page.component';
import { PatientComponent } from './patient/patient.component';
import { PatientModifyComponent } from './patient-modify/patient-modify.component';
import { CaregiverModifyComponent } from './caregiver-modify/caregiver-modify.component';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { MedicationPlanComponent } from './medication-plan/medication-plan.component';
@NgModule({
  declarations: [
    AppComponent,
    DrugsComponent,
    DrugModifyComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    ErrorHandlingComponent,
    CaregiverPageComponent,
    PatientPageComponent,
    DoctorPageComponent,
    PatientComponent,
    PatientModifyComponent,
    CaregiverModifyComponent,
    CaregiverComponent,
    MedicationPlanComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
    MatPaginatorModule,
    RouterModule.forRoot([
      {
        path: 'drug',
        component: DrugsComponent
      },
      {path: '', component: LoginComponent},
      {path: 'doctorPage', component: DoctorPageComponent},
      {path: 'caregiverPage', component: CaregiverPageComponent},
      {path: 'patientPage', component: PatientPageComponent},
      {path: 'patients', component: PatientComponent},
      {path: 'caregiver', component: CaregiverComponent},
      {path: 'drugs/modifyDrug', component: DrugModifyComponent },
      {path: 'patient/modifyPatient', component: PatientModifyComponent },
      {path: 'caregiver/modifyCaregiver', component: CaregiverModifyComponent },
      {path: 'medicationPlan', component: MedicationPlanComponent },
      ]),
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule
  ],
  entryComponents: [
    ErrorHandlingComponent
  ],
  exports: [
    MatPaginatorModule,
    MatTableModule,
    ErrorHandlingComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
