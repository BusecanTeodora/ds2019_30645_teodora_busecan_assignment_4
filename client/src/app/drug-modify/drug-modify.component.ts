import { Component, OnInit } from '@angular/core';
import {DrugService} from '../services/drug.service';
import {Drug} from '../model/Drug';
import {Router} from '@angular/router';

@Component({
  selector: 'app-drug-modify',
  templateUrl: './drug-modify.component.html',
  styleUrls: ['./drug-modify.component.css']
})
export class DrugModifyComponent implements OnInit {
  drug: Drug;

  constructor(private router: Router, private drugService: DrugService) { }

  ngOnInit() {
    this.drug = JSON.parse(localStorage.getItem('drug_modify'));
    console.log(this.drug);
  }
  confirmModify() {
      this.drugService.modifyDrug(this.drug).subscribe(data => this.router.navigate(['drug']));

  }


}
