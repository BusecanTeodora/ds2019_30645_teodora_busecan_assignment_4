package com.example.springdemo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake")
public class Intake implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idintake", unique = true, nullable = false)
    private Integer idintake;

    @ManyToOne
    @JoinColumn(name = "idpatient")
    private Patient patient;


    @Column(name = "period")
    private Integer period;

    @Column(name = "start_time")
    private LocalDate start_time;

    public Intake(Integer idintake, Integer period) {
        this.idintake = idintake;

        this.period = period;
    }

    public Intake(Integer idintake, Integer period, LocalDate start_time) {
        this.idintake = idintake;
        this.period = period;
        this.start_time = start_time;
    }

    public Intake() {
    }

    public LocalDate getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDate start_time) {
        this.start_time = start_time;
    }

    public Integer getIdintake() {
        return idintake;
    }

    public void setIdintake(Integer idintake) {
        this.idintake = idintake;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }
}
