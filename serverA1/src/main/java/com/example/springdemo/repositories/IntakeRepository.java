package com.example.springdemo.repositories;

import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IntakeRepository extends JpaRepository<Intake, Integer> {
    List<Intake> findIntakeByPatient(Patient idpatient);
   // List<Intake> findIntakeByidintake(Integer idintake);
}
