package com.example.springdemo.dto;

import java.util.Objects;

public class DoctorDTO {

    private Integer iddoctor;
    private String name;

    private UserDTO userDTO;

    public DoctorDTO() {
    }

    public DoctorDTO(Integer iddoctor, String name, UserDTO userDTO) {
        this.iddoctor = iddoctor;
        this.name = name;
        this.userDTO = userDTO;
    }

    public Integer getIddoctor() {
        return iddoctor;
    }

    public void setIddoctor(Integer iddoctor) {
        this.iddoctor = iddoctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(iddoctor, doctorDTO.iddoctor) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(userDTO, doctorDTO.userDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iddoctor, name, userDTO);
    }
}
