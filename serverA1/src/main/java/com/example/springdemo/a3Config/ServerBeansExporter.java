package com.example.springdemo.a3Config;

import com.example.springdemo.services.A3Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

@Configuration
public class ServerBeansExporter {

    @Bean
    RmiServiceExporter exporter(A3Service implementation) {
        Class<A3Service> serviceInterface = A3Service.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(implementation);
        exporter.setServiceName(serviceInterface.getSimpleName());
        exporter.setRegistryPort(1099);
        return exporter;
    }
}
