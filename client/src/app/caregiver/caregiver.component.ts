import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Caregiver} from '../model/Caregiver';
import {CaregiverService} from '../services/caregiver.service';

@Component({
  selector: 'app-users',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  caregivers: Caregiver[] = [];
  caregiverInsert: Caregiver;
  registerForm: FormGroup;
  submitted: boolean;

  constructor(private router: Router, private caregiverService: CaregiverService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updateCaregiverTable();
  }

  updateCaregiverTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      birthday: ['', [Validators.required]],
      address: ['', Validators.required],
      gender: ['', Validators.required],
      idDoctor: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.caregiverInsert = new Caregiver();
    this.caregiverService.getCaregivers()
      .subscribe(data => {
        this.caregivers = data;
      });
  }

  get requestedForm() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    this.caregiverInsert.name = this.registerForm.get('name').value;
    this.caregiverInsert.birthday = this.registerForm.get('birthday').value;
    this.caregiverInsert.address = this.registerForm.get('address').value;
    this.caregiverInsert.gender = this.registerForm.get('gender').value;
    this.caregiverInsert.idDoctor = this.registerForm.get('idDoctor').value;
    this.caregiverInsert.idUser = 4;
    this.caregiverInsert.username = this.registerForm.get('username').value;
    this.caregiverInsert.password = this.registerForm.get('password').value;
    console.log(this.caregiverInsert.idDoctor);
    this.caregiverService.insertCaregiver(this.caregiverInsert).subscribe(data => this.updateCaregiverTable());
  }

  deleteCaregiver(id: number) {
    for (const caregiver of this.caregivers) {
      if (caregiver.idcaregiver === id) {
        console.log(caregiver);
        this.caregiverService.deleteCaregiver(caregiver).subscribe(data => this.updateCaregiverTable());
      }
    }
  }

  modifyCaregiver(user: any) {
    localStorage.setItem('caregiver_modify', JSON.stringify(user));
    this.router.navigate(['caregiver/modifyCaregiver']);
  }
}
