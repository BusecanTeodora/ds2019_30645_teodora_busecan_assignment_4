import {Component, Input} from '@angular/core';
import {LoginService} from '../services/login.service';
import { Router } from '@angular/router';
import {UserService} from '../services/user.service';
import {Caregiver} from '../model/Caregiver';
import {Patient} from '../model/Patient';
import {Doctor} from '../model/Doctor';
import {UserLogIn} from '../model/UserLogIn';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: UserLogIn;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private userService: UserService) {
    this.user = new UserLogIn();
  }

  onLoginSubmit() {
    this.loginService.logIn(this.user).subscribe(result => {
      if (result === 1) {
        let doctor: Doctor;
        this.userService.getDoctor(this.user.username).subscribe(data => {
          doctor = data;
        }).add(() => {
        this.router.navigate(['/doctorPage']);
        }); } else {
        if (result === 2) {
          let caregiver: Caregiver;
          this.userService.getCaregiver(this.user.username).subscribe(data => {
            console.log(data);
            caregiver = data;
          }).add(() => {
            localStorage.setItem('user', JSON.stringify(caregiver));
            this.router.navigate(['/caregiverPage']);
          });
        } else {
          if (result === 3) {
            let patient: Patient;
            this.userService.getPatient(this.user.username).subscribe(data => {
              patient = data;
            }).add(() => {
              localStorage.setItem('user', JSON.stringify(patient));
              this.router.navigate(['/patientPage']);
            });

          } else {
            window.alert('User credentials wrong');
          }
        }
      }

    });
  }

}
