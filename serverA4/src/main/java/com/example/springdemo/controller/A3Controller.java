package com.example.springdemo.controller;

import com.example.springdemo.Client;
import com.example.springdemo.a3Config.ConfigureBeans;
import com.example.springdemo.dto.IntakeDTOView;
import com.example.springdemo.entities.Drug;
import com.example.springdemo.entities.TableModelDTO;
import com.example.springdemo.services.A3Service;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;


@Component
@EnableScheduling
@Import(ConfigureBeans.class)
public class A3Controller implements Initializable {

    @FXML
    Label dateLabel;
    @FXML
    Text error;
    @FXML
    TableView<TableModelDTO> medPlanTable;
    @FXML
    TableColumn<TableModelDTO, String> nameTable;
    @FXML TableColumn<TableModelDTO, String> sideEffectTable;
    @FXML TableColumn<TableModelDTO, String> dosageTable;
    @FXML TableColumn<TableModelDTO, String> statusTable;
    @FXML TableColumn<TableModelDTO, String> whenTable;

    private TableModelDTO tableModelDTOSelected;


    @Autowired
   private A3Service a3Service;
    private static List<TableModelDTO> intakes;
    private String partOfDay;

    public A3Controller() {
        a3Service = Client.service;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initClock();
        if (LogInController.iduser != -1) {
            List<IntakeDTOView> intakes = a3Service.getIntakes(LogInController.iduser);
            setMedPlan(intakes);
        }
        nameTable.setCellValueFactory(new PropertyValueFactory<>("medicationName"));
        sideEffectTable.setCellValueFactory(new PropertyValueFactory<>("medicationSideEffects"));
        dosageTable.setCellValueFactory(new PropertyValueFactory<>("dosage"));
        statusTable.setCellValueFactory(new PropertyValueFactory<>("status"));
        whenTable.setCellValueFactory(new PropertyValueFactory<>("partOfDay"));

        medPlanTable.getSelectionModel().selectedItemProperty().addListener((observable,vechi, nou) ->{
            tableModelDTOSelected = nou;

        } );
        seeCurrentTime();
        setTableItems(intakes);


    }

    private void initClock() {
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            dateLabel.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    @Scheduled(cron = "0 30 11 * * ?")
    private void getMedications() {
        if (LogInController.iduser != -1) {
           List<IntakeDTOView> intakes =a3Service.getIntakes(LogInController.iduser);
            setMedPlan(intakes);
        }
    }

    private void setMedPlan(List<IntakeDTOView> intakeDTOViews) {
        intakes = new ArrayList<>();
        for (IntakeDTOView intakeDTOView : intakeDTOViews) {
            for(int i=0;i<intakeDTOView.getDrugDTOs().size();i++){
                String nameDrug = intakeDTOView.getDrugDTOs().get(i);
                Drug drug =a3Service.getDrug(nameDrug);
                String[] intervals = intakeDTOView.getIntake_intervals().get(i).split("-");
                if( intervals[0].equals("1") ) {
                    intakes.add(new TableModelDTO( drug.getName(),drug.getSideeffect(), drug.getDosage(), "WAITING", "MORNING"));
                }
                if( intervals[1].equals("1") ) {
                    intakes.add(new TableModelDTO( drug.getName(),drug.getSideeffect(), drug.getDosage(), "WAITING", "NOON"));
                }
                if( intervals[2].equals("1") ) {
                    intakes.add(new TableModelDTO( drug.getName(),drug.getSideeffect(), drug.getDosage(), "WAITING", "EVENING"));
                }
            }
        }

    }

    public void seeCurrentTime(){
        if(LocalTime.now().isBefore(LocalTime.of(13, 0)) && LocalTime.now().isAfter(LocalTime.of(7,0))) {
            partOfDay = "MORNING";
        } else if(LocalTime.now().isBefore(LocalTime.of(17, 0)) && LocalTime.now().isAfter(LocalTime.of(13, 0))) {
            partOfDay = "NOON";
        } else {
            partOfDay = "EVENING";
        }
    }


    private void setTableItems(List<TableModelDTO> items) {
        medPlanTable.getItems().setAll(items);
    }

    public void takeButton() {
        if(!partOfDay.equals(tableModelDTOSelected.getPartOfDay())){
            error.setText("You can take only " + partOfDay + "  pils!");
            return;
        }
        else {
            error.setText("");
        }
        if(!tableModelDTOSelected.getStatus().equals("WAITING")){
            error.setText("Nu poti lua aceasta pastila!");
            return;
        }
        else {
            error.setText("");
        }

        tableModelDTOSelected.setStatus("TAKEN");

        Client.service.intakeLog("TAKEn " + tableModelDTOSelected.getMedicationName() + " a fost luat la/in " + tableModelDTOSelected.getPartOfDay() + " din data: " + LocalDateTime.now() +".", LogInController.iduser);
        intakes.remove(tableModelDTOSelected);
        medPlanTable.getItems().setAll(intakes);
        medPlanTable.refresh();
    }

    public void onlyEvening(){
            List<TableModelDTO> morningMedications = intakes;
            List<TableModelDTO> eveningMedicationOnly = new ArrayList<>();
            for(TableModelDTO tableModelDTO:morningMedications) {
                if(tableModelDTO.getPartOfDay().equals("EVENING")){
                    eveningMedicationOnly.add(tableModelDTO);
                }
            }
        medPlanTable.getItems().setAll(eveningMedicationOnly);
        medPlanTable.refresh();
    }
    public void onlyMorning(){
        List<TableModelDTO> morningMedications = intakes;
        List<TableModelDTO> eveningMedicationOnly = new ArrayList<>();
        for(TableModelDTO tableModelDTO:morningMedications) {
            if(tableModelDTO.getPartOfDay().equals("MORNING")){
                eveningMedicationOnly.add(tableModelDTO);
            }
        }
        medPlanTable.getItems().setAll(eveningMedicationOnly);
        medPlanTable.refresh();
    }
    public void onlyNoon(){
        List<TableModelDTO> morningMedications = intakes;
        List<TableModelDTO> eveningMedicationOnly = new ArrayList<>();
        for(TableModelDTO tableModelDTO:morningMedications) {
            if(tableModelDTO.getPartOfDay().equals("NOON")){
                eveningMedicationOnly.add(tableModelDTO);
            }
        }
        medPlanTable.getItems().setAll(eveningMedicationOnly);
        medPlanTable.refresh();
    }
    public void all(){
        List<TableModelDTO> morningMedications = intakes;
        medPlanTable.getItems().setAll(intakes);
        medPlanTable.refresh();
    }

    @Scheduled(cron = "0 35 11 * * ?")
    private void checkMorningMedicationStatus() {
        List<TableModelDTO> morningMedications = medPlanTable.getItems().stream().filter(element -> element.getPartOfDay().equals("MORNING") && element.getStatus().equals("WAITING")).collect(Collectors.toList());
        updateMedications(morningMedications);
        partOfDay = "NOON";
    }

    @Scheduled(cron = "0 0 18 * * ?")
    private void checkNoonMedicationStatus() {
        List<TableModelDTO> morningMedications = medPlanTable.getItems().stream().filter(element -> element.getPartOfDay().equals("NOON") && element.getStatus().equals("WAITING")).collect(Collectors.toList());
        updateMedications(morningMedications);
        partOfDay = "EVENING";
    }


    @Scheduled(cron = "0 29  11 * * ?")
    private void checkEveningMedicationStatus() {
        List<TableModelDTO> morningMedications = medPlanTable.getItems().stream().filter(element -> element.getPartOfDay().equals("EVENING") && element.getStatus().equals("WAITING")).collect(Collectors.toList());
        updateMedications(morningMedications);
        partOfDay = "MORNING";
    }

    private void updateMedications(List<TableModelDTO> medications) {
        medications.forEach( element -> element.setStatus("EXPIRED"));
        medPlanTable.refresh();
        medications.forEach( element ->a3Service.intakeLog("EXPIRED:Medicamentul: " + element.getMedicationName() + "/ " + element.getDosage() + " nu a fost luat in/la " + element.getPartOfDay() + " din data: " + LocalDateTime.now() +".", LogInController.iduser));
    }

}
