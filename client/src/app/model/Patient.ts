export class Patient {
  idpatient: number;
  idCaregiver: number;
  idUser: number;
  name: string;
  birthday: string;
  address: string;
  gender: string;
  username: string;
  password: string;

}
