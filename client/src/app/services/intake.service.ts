import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {REST_API} from '../common/API';
import 'rxjs-compat/add/operator/catch';
import {MatDialog} from '@angular/material';
import {Intake} from '../model/Intake';


@Injectable({
  providedIn: 'root'
})
export class IntakeService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getIntake(idPatient: number) {
    return this.http.post<Intake[]>(REST_API + 'intake', idPatient);
  }

  addIntake(intake: Intake) {
   return this.http.post(REST_API + 'intake/insert', intake);

  }
}
