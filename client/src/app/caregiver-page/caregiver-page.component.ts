import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Patient} from '../model/Patient';
import {CaregiverPageService} from '../services/caregiver-page.service';
import {Caregiver} from '../model/Caregiver';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Component({
  selector: 'app-users',
  templateUrl: './caregiver-page.component.html',
  styleUrls: ['./caregiver-page.component.css']
})
export class CaregiverPageComponent implements OnInit {
  patients: Patient[] = [];
  caregiver: Caregiver;

  constructor(private router: Router, private caregiverPageService: CaregiverPageService) {
  }

  ngOnInit() {
    this.caregiver = JSON.parse(localStorage.getItem('user'));
    this.caregiverPageService.getPatients(this.caregiver.idcaregiver)
      .subscribe(data => {
        this.patients = data;
      });
    this.connect();
  }


  logOut() {
    this.router.navigate(['']);
  }
   showMessageOutput(messageOutput) {
    console.log(messageOutput);
    alert(messageOutput.message);
  }
   connect() {

     const socket = new SockJS('http://localhost:8080/socket');
     const stompClient = Stomp.over(socket);
     const that = this;
     stompClient.connect({}, frame => {
       stompClient.subscribe('/topic/socket/caregiver/' + this.caregiver.idcaregiver, messageOutput => {
         that.showMessageOutput(JSON.parse(messageOutput.body));
       });
     });
   }

}
