export class Caregiver {
  idcaregiver: number;
  name: string;
  birthday: string;
  address: string;
  gender: string;
  idDoctor: number;
  idUser: number;
  username: string;
  password: string;
}
