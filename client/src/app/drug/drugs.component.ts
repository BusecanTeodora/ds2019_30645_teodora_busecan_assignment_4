import {Component, OnInit} from '@angular/core';
import {Drug} from '../model/Drug';
import {DrugService} from '../services/drug.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './drugs.component.html',
  styleUrls: ['./drugs.component.css']
})
export class DrugsComponent implements OnInit {

  drugs: Drug[] = [];
  drugInsert: Drug;
  registerForm: FormGroup;
  submitted: boolean;

  constructor(private router: Router, private drugService: DrugService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updateDrugTable();
  }

  updateDrugTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      sideeffect: ['', [Validators.required]],
      dosage: ['', Validators.required]
    });
    this.drugInsert = new Drug();
    this.drugService.getDrugs()
      .subscribe(data => {
        this.drugs = data;
      });
  }

  get requestedForm() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.drugInsert.name = this.registerForm.get('name').value;
    this.drugInsert.sideeffect = this.registerForm.get('sideeffect').value;
    this.drugInsert.dosage = this.registerForm.get('dosage').value;
    this.drugService.insertDrug(this.drugInsert).subscribe(data => this.updateDrugTable());
  }

  deleteDrug(id: number) {
    for (const drug of this.drugs) {
      if (drug.iddrug === id) {
        console.log(drug);
        this.drugService.deleteDrug(drug).subscribe(data => this.updateDrugTable());
      }
    }
   // this.updateDrugTable();
  }

  modifyDrug(user: any) {
    localStorage.setItem('drug_modify', JSON.stringify(user));
    this.router.navigate(['drugs/modifyDrug']);
  }
}
