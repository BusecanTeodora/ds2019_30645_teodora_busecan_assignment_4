import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverModifyComponent } from './caregiver-modify.component';

describe('CaregiverModifyComponent', () => {
  let component: CaregiverModifyComponent;
  let fixture: ComponentFixture<CaregiverModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
