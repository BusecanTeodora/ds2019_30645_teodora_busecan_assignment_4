package com.example.springdemo.entities;

public class TableModelDTO {

    private String medicationName;
    private String medicationSideEffects;
    private int dosage;
    private String status;
    private String partOfDay;

    public TableModelDTO( String medicationName, String medicationSideEffects, int dosage, String status, String partOfDay) {
        this.medicationName = medicationName;
        this.medicationSideEffects = medicationSideEffects;
        this.dosage = dosage;
        this.status = status;
        this.partOfDay = partOfDay;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getMedicationSideEffects() {
        return medicationSideEffects;
    }

    public void setMedicationSideEffects(String medicationSideEffects) {
        this.medicationSideEffects = medicationSideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPartOfDay() {
        return partOfDay;
    }

    public void setPartOfDay(String partOfDay) {
        this.partOfDay = partOfDay;
    }

}
