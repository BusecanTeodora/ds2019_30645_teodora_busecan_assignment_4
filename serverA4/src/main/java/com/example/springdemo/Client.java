package com.example.springdemo;


import com.example.springdemo.a3Config.ConfigureBeans;
import com.example.springdemo.services.A3Service;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;

@SpringBootApplication
@Import(ConfigureBeans.class)
public class Client extends Application {

    private Parent parent;
    public static A3Service service;
    public static void main(String[] args){
        //service= SpringApplication
                //.run(Client.class, args).getBean(A3Service.class);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(parent));
        primaryStage.show();
    }

    @Override
    public void init() throws IOException {
        ApplicationContext springContext = SpringApplication.run(Client.class);
        service = springContext.getBean(A3Service.class);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/logInPage.fxml"));
        loader.setControllerFactory(springContext::getBean);
        parent = loader.load();
    }
}
