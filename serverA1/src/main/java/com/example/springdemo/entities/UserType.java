package com.example.springdemo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "usertype")
public class UserType implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "usertype", unique = true, nullable = false)
    private Integer userType;

    @OneToMany(mappedBy = "usertype", cascade = CascadeType.ALL)
    private Set<User> users;

    public UserType() {
    }

    public UserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
}
