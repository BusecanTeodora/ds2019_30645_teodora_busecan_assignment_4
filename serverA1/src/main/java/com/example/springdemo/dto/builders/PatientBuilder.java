package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.*;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;

public class PatientBuilder {

    public PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        CaregiverDTO caregiverDTO = new CaregiverDTO();
        UserTypeDTO userTypeDTOCareg = new UserTypeDTO();
        UserDTO userDTOCaregiv = new UserDTO();

        DoctorDTO doctorDTO = new DoctorDTO();
        UserDTO userDTODoctor = new UserDTO();
        UserTypeDTO userTypeDTODoctor = new UserTypeDTO();

        UserTypeDTO userTypeDTO = new UserTypeDTO();
        UserDTO userDTO = new UserDTO();

        userTypeDTOCareg.setUsertype(patient.getCaregiver().getUser().getUserType().getUserType());
        userTypeDTODoctor.setUsertype(patient.getCaregiver().getDoctor().getUser().getUserType().getUserType());

        userDTODoctor.setUserTypeDTO(userTypeDTODoctor);
        userDTODoctor.setIduser(patient.getCaregiver().getDoctor().getUser().getIduser());
        userDTODoctor.setUsername(patient.getCaregiver().getDoctor().getUser().getUsername());
        userDTODoctor.setPassword(patient.getCaregiver().getDoctor().getUser().getPassword());

        doctorDTO.setUserDTO(userDTODoctor);
        doctorDTO.setName(patient.getCaregiver().getDoctor().getName());
        doctorDTO.setIddoctor(patient.getCaregiver().getDoctor().getIddoctor());

        userDTOCaregiv.setUserTypeDTO(userTypeDTOCareg);
        userDTOCaregiv.setIduser(patient.getCaregiver().getUser().getIduser());
        userDTOCaregiv.setUsername(patient.getCaregiver().getUser().getUsername());
        userDTOCaregiv.setPassword(patient.getCaregiver().getUser().getPassword());

        caregiverDTO.setAddress(patient.getCaregiver().getAddress());
        caregiverDTO.setBirthday(patient.getCaregiver().getDate());
        caregiverDTO.setGender(patient.getCaregiver().getGender());
        caregiverDTO.setName(patient.getCaregiver().getName());
        caregiverDTO.setIdcaregiver(patient.getCaregiver().getIdcaregiver());
        caregiverDTO.setDoctorDTO(doctorDTO);
        caregiverDTO.setUserDTO(userDTOCaregiv);

        userTypeDTO.setUsertype(patient.getUser().getUserType().getUserType());

        userDTO.setPassword(patient.getUser().getPassword());
        userDTO.setUsername(patient.getUser().getUsername());
        userDTO.setIduser(patient.getUser().getIduser());
        userDTO.setUserTypeDTO(userTypeDTO);

        return new PatientDTO(patient.getIdpatient(),patient.getName(),patient.getDate(),patient.getAddress(),patient.getGender(),caregiverDTO,userDTO);
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getIdpatient(),
                patientDTO.getName(),
                patientDTO.getBirthday(),
                patientDTO.getAddress(),
                patientDTO.getGender());
    }

}
