package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.UserTypeDTO;
import com.example.springdemo.entities.UserType;

public class UserTypeBuilder {

    public UserTypeBuilder() {
    }

    public static UserTypeDTO generateDTOFromEntity(UserType userType){
        return new UserTypeDTO(userType.getUserType());
    }

    public static UserType generateEntityFromDTO(UserTypeDTO userTypeDTO){
        return new UserType(userTypeDTO.getUsertype());
    }
}
