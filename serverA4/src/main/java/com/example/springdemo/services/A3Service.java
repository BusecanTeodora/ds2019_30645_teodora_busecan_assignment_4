package com.example.springdemo.services;


import com.example.springdemo.dto.IntakeDTOView;
import com.example.springdemo.entities.Drug;

import java.util.List;

public interface A3Service  {
    //String getString(String pickUpLocation);
    Integer isLoggedIn(String username, String password);
    List<IntakeDTOView> getIntakes(Integer clientId);
    void intakeLog(String log, Integer idpatient);
    Drug getDrug(String name);
}
