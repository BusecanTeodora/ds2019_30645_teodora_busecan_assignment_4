import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugModifyComponent } from './drug-modify.component';

describe('DrugModifyComponent', () => {
  let component: DrugModifyComponent;
  let fixture: ComponentFixture<DrugModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
