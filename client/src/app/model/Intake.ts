export class Intake {
  idintake: number;
  period: string;
  intake_intervals: string[];
  idPatient: number;
  start_time: Date;
  drugDTOs: string[];

}
